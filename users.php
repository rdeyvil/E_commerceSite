
 <?php
  require_once $_SERVER['DOCUMENT_ROOT'].'/tutorial/core/init.php';
  include 'includes/head.php';
include 'includes/navigation.php'; ?>

	<div class="row">
		<div class="col-md-6">
<h2 class="text-center">REGISTER</h2><hr>

		<form action="users.php" method="post">
			<div class="form-group col-md-6">
				<label for="name">Full Name:</label>
				<input type="text" name="name" id="name" class="form-control" value="<?=$name?>">
			</div>	
			<div class="form-group col-md-6">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" class="form-control" value="<?=$email?>">
			</div>
			<div class="form-group col-md-6">
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" class="form-control" value="<?=$password?>">
			</div>
			<div class="form-group col-md-6">
				<label for="confirm">Confirm Password:</label>
				<input type="password" name="confirm" id="confirm" class="form-control" value="<?=$confirm?>">
			</div>
			<div class="form-group col-md-6">
				<label for="name">Permissions:</label>
				<select class="form-control" name="permissions">
					<option value=""<?=(($permissions == '')?' selected':'');?>></option>
					<option value="editor"<?=(($permissions == 'editor')?' selected':'');?>>Editor</option>
					<option value="admin,editor"<?=(($permissions == 'admin,editor')?' selected':'');?>>Admin</option>
				</select>
			</div>
			<div class="form-group col-md-6 text-right" style="margin-top:35px;">
				<a href="users.php" class="btn btn-default btn-danger">Cancel</a>
				<input type="submit" value="Add User" class="btn btn-primary">
			</div>
		</form>

			
		</div>
	</div>
	





 <?php include 'includes/footer.php'; ?>}
