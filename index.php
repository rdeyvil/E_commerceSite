<?php 
require_once 'core/init.php';
include 'includes/head.php'; 
include 'includes/navigation.php'; 
include 'includes/headerfull.php';
include 'includes/leftbar.php';

$sql = "SELECT * FROM products WHERE featured = 1 ORDER BY rand()";
$featured = $db->query($sql);
?>





<!.. main content.../>

<div class="col-md-8">
	
	<div class="row">
		<h2 class="text-center" style="font-size: 70px;" >Featured Products</h2>
			<?php while($product= mysqli_fetch_assoc($featured)):?>


		<div class="col-md-3">
			<h4><?php echo $product['title']; ?></h4>
			<img src="<?php echo $product['image']; ?>" alt="<?php echo $product['title']; ?>" class="img-thumb"/>
			<p class="list-price text-danger">List price<s>RS:<?php echo $product['list_price']; ?></s></p>
			<p class="price">Our Price Rs:<?php echo $product['price']; ?></p>
			<button type="botton" class="btn btn-sm btn-success"  data-toggle="modal" onclick="recommendation_modal(<?php echo $product['id']; ?>)">Details</button>
			<button class="btn btn-warning" data-toggle="modal" onclick="detailsmodal(<?php echo $product['id']; ?>)"><span class="glyphicon glyphicon-shopping-cart"></span>Cart</button>
			
		</div>
			<?php endwhile; ?>
	</div>	
</div>



<?php
include 'includes/rightbar.php';
include 'includes/footer.php';
?>
