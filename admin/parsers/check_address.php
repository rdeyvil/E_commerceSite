<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/tutorial/core/init.php';
$name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$state = sanitize($_POST['state']);
$city = sanitize($_POST['city']);
$street = sanitize($_POST['street']);
$phone = sanitize($_POST['phone']);
$errors = array();
$required = array(
	'full_name' => 'Full Name',
	'email'     => 'Email',
	'state'     => 'State',
	'city'      => 'City',
	'phone'    => 'Phone',
);


//check if all required is filled out
foreach($required as $f => $d){
	if(empty($_POST[$f]) || $_POST[$f] == ''){
		$errors[] = $d.' is required.';
	}
}


//check if valid email address
if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
	$errors[] = 'Plese enter a valid email';
}

if (!empty($errors)) {
	echo display_errors($errors);
}else{
	echo 'passed';
}

?>
