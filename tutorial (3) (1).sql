-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 11:07 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand`) VALUES
(1, 'puma'),
(2, 'Polo'),
(3, 'nike'),
(4, 'addidas	'),
(5, '	SS	'),
(6, '	GM'),
(7, '	EA	sports');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL,
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `shipped` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `items`, `expire_date`, `paid`, `shipped`) VALUES
(44, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-02-09 08:06:22', 0, 1),
(45, '[[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]]', '2018-02-09 08:38:20', 0, 0),
(46, '[[{\"id\":\"15\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":\"1\"},{\"id\":\"16\",\"size\":\"medium\",\"quantity\":\"1\"}],[{\"id\":\"15\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":\"1\"},{\"id\":\"16\",\"size\":\"medium\",\"quantity\":\"1\"}],[{\"id\":\"15\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":\"1\"},{\"id\":\"16\",\"size\":\"medium\",\"quantity\":\"1\"}]]', '2018-02-10 06:35:39', 0, 0),
(47, '[{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"2\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"},[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]]', '2018-02-10 07:14:54', 0, 0),
(48, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"},[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"15\",\"size\":\"large\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}],[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"15\",\"size\":\"large\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}],[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"15\",\"size\":\"large\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}],[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"15\",\"size\":\"large\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"3\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}]]', '2018-02-10 07:45:05', 0, 0),
(49, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}]', '2018-02-10 07:45:44', 0, 0),
(50, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}]', '2018-02-10 07:46:20', 0, 0),
(51, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}]', '2018-02-10 07:47:11', 0, 0),
(52, '[{\"id\":\"16\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-02-10 09:00:29', 0, 0),
(53, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-02-13 10:36:49', 0, 1),
(54, '[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"2\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":2}]', '2018-02-13 16:48:14', 0, 1),
(56, '[{\"id\":\"12\",\"size\":\"medium\",\"quantity\":2}]', '2018-02-14 04:33:05', 0, 0),
(57, '[{\"id\":\"\",\"size\":\"\",\"quantity\":\"\"},{\"id\":\"25\",\"size\":\"Standard\",\"quantity\":2}]', '2018-02-14 06:53:17', 0, 0),
(58, '[{\"id\":\"\",\"size\":\"\",\"quantity\":\"\"}]', '2018-02-14 06:59:44', 0, 0),
(59, '[{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":\"1\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"2\"}]', '2018-02-14 08:06:08', 0, 1),
(60, '[{\"id\":\"22\",\"size\":\"S\",\"quantity\":1},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"1\"}]', '2018-02-14 12:01:38', 0, 0),
(61, '[{\"id\":\"19\",\"size\":\"41\",\"quantity\":2}]', '2018-02-14 12:08:59', 0, 1),
(62, '[{\"id\":\"17\",\"size\":\"s\",\"quantity\":2},{\"id\":\"21\",\"size\":\"L\",\"quantity\":\"2\"}]', '2018-02-14 16:44:10', 0, 1),
(63, '[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"10\"},{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"29\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"19\",\"size\":\"40\",\"quantity\":2}]', '2018-02-15 05:17:09', 0, 1),
(64, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-02-15 05:21:13', 0, 1),
(65, '[{\"id\":\"16\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":\"2\"}]', '2018-02-15 05:51:08', 0, 1),
(66, '[{\"id\":\"27\",\"size\":\"M\",\"quantity\":\"1\"},{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"26\",\"size\":\"M\",\"quantity\":\"2\"},{\"id\":\"16\",\"size\":\"small\",\"quantity\":\"2\"}]', '2018-02-17 03:24:05', 0, 1),
(67, '[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"}]', '2018-02-24 12:27:06', 0, 0),
(68, '[{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"1\"}]', '2018-07-15 03:59:16', 0, 1),
(69, '[{\"id\":\"12\",\"size\":\"medium\",\"quantity\":\"1\"},{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-08-09 06:15:48', 0, 1),
(70, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"}]', '2018-08-09 06:24:47', 0, 1),
(73, '[{\"id\":\"17\",\"size\":\"s\",\"quantity\":\"1\"},{\"id\":\"23\",\"size\":\"40\",\"quantity\":\"1\"}]', '2018-08-12 09:20:11', 0, 1),
(74, '[{\"id\":\"21\",\"size\":\"S\",\"quantity\":\"1\"}]', '2018-08-12 09:35:21', 0, 0),
(76, '[{\"id\":\"21\",\"size\":\"L\",\"quantity\":1}]', '2018-08-12 09:59:19', 0, 0),
(77, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"1\"},{\"id\":\"23\",\"size\":\"41\",\"quantity\":\"1\"}]', '2018-09-08 07:46:00', 0, 0),
(78, '[{\"id\":\"16\",\"size\":\"medium\",\"quantity\":\"1\"}]', '2018-09-08 09:03:37', 0, 0),
(79, '[{\"id\":\"16\",\"size\":\"small\",\"quantity\":\"1\"},{\"id\":\"14\",\"size\":\"OFFICIAL\",\"quantity\":1}]', '2018-11-29 05:43:35', 0, 0),
(80, '[{\"id\":\"13\",\"size\":\"Official\",\"quantity\":\"1\"}]', '2018-12-15 10:30:03', 0, 0),
(81, '[{\"id\":\"19\",\"size\":\"41\",\"quantity\":\"1\"}]', '2018-12-20 07:45:39', 0, 1),
(82, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"3\"},{\"id\":\"23\",\"size\":\"40\",\"quantity\":\"1\"}]', '2018-12-22 10:26:14', 0, 0),
(83, '[{\"id\":\"12\",\"size\":\"small\",\"quantity\":\"2\"}]', '2018-12-22 10:33:52', 0, 0),
(84, '[{\"id\":\"13\",\"size\":\"Official\",\"quantity\":\"1\"}]', '2018-12-22 11:06:19', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `parent`) VALUES
(49, ' Football', 0),
(55, ' Ball', 49),
(63, ' Gloves', 49),
(68, ' Boot ', 49),
(70, ' Jersey', 49),
(71, ' Cricket', 0),
(72, ' Bat', 71),
(73, ' Ball', 71),
(74, ' Gloves', 71),
(75, ' Jersey ', 71),
(76, ' Shoes', 71),
(77, '  Basketball', 0),
(78, ' Ball', 77),
(79, ' Jersey ', 77),
(80, ' Shoes', 77),
(81, ' Nets ', 77),
(82, ' Futsal Boot', 49),
(89, ' Indoor Sports', 0),
(90, ' Carramboard', 89),
(91, ' Chess', 89),
(93, ' Trophies', 0),
(94, ' Trophie', 93),
(95, ' Medals', 93),
(96, ' Table Tennis bat', 89),
(97, ' Baseball', 0),
(99, ' Bat', 97),
(100, ' others', 0),
(101, ' Swimming', 100);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `list_price` decimal(10,0) NOT NULL,
  `brand` int(11) NOT NULL,
  `categories` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `sizes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `list_price`, `brand`, `categories`, `image`, `description`, `featured`, `sizes`, `deleted`) VALUES
(12, 'Chelsea poiuy', '1000', '1600', 3, '70', '/tutorial/images/products/92c76ce8c5ccd5ff456515cb271345c1.jpg', 'Authentic Chelsea Jersey.', 1, 'small:14:4,medium:13:', 0),
(13, 'Rugby Ball', '2000', '1800', 3, '78', '/tutorial/images/products/993ca0d325f39eed67da192f8b5b5c66.jpg', 'Official Gilbert Rugby Ball of 2017... ', 1, 'Official:17:8', 0),
(14, 'BasketBall', '2000', '2567', 1, '78', '/tutorial/images/products/4573f1555db5c09ee9f7f64559fa3d1f.jpg', 'NBA Official Ball..', 1, 'OFFICIAL:8:6', 0),
(15, 'Baseball Gloves', '4500', '4400', 4, '99', '/tutorial/images/products/3c2755cff1014b42ff6295a685471586.jpg', 'Nike gloves .\r\n', 0, 'medium:5:4,large:6:4', 0),
(16, 'Manchester United', '4500', '5000', 4, '70', '/tutorial/images/products/3c36b4061a6d4479efa9c9630a97c22c.png', 'Manchester United Jersey .. High Quality Jersey.', 0, 'small:6:2,medium:2:4', 0),
(17, 'Football', '3333', '4444', 3, '55', '/tutorial/images/products/4e7f9d7b838f0469a589134a23b00b12.jpg', 'niceone', 0, 's:4:3', 0),
(18, 'bat', '5000', '6000', 5, '99', '/tutorial/images/products/5c0090d29c163ffbc693657f18aef4f4.jpg', 'High Quality bat now available. Grab it fast. ', 0, 'L:5:2,S:4:3', 0),
(19, 'Football boot prince', '2500', '2999', 4, '68', '/tutorial/images/products/c6cca0693cfb6dc005f284d0d4cd110c.png', 'New models out now...', 1, '40:14:4,41:0:3,42:9:2,43:9:3', 0),
(20, 'Cricket Ball', '350', '400', 5, '73', '/tutorial/images/products/4428769988d30fa9ea01cc020ee7358c.png', '', 1, 'Standard:10:8', 0),
(21, 'Cricket Bat', '5000', '5555', 5, '72', '/tutorial/images/products/b23aedc32878c632b0ba481df6cac036.png', 'Both Small and Large Out now', 0, 'L:2:2,S:2:1', 0),
(22, 'Trophie', '4000', '4500', 5, '94', '/tutorial/images/products/07a95043ec0c88942ff3d479baf6496c.png', 'metallic body cased trophies...', 0, 'large:5:2', 0),
(23, 'Shoes', '3500', '4000', 4, '80', '/tutorial/images/products/9b3680d74ad2703528c9e8ab027d4895.png', 'High Quality basketball shoe', 0, '40:6:3,41:2:3,42:3:3,43:2:3', 0),
(24, 'Batsman gloves', '1000', '1300', 5, '74', '/tutorial/images/products/0910e515188c1d7414f0cb8f29774626.jpg', 'nice one', 0, 'Standard:11:8', 0),
(25, 'Cricket Keeper Gloves', '1600', '2000', 6, '74', '/tutorial/images/products/2b3ffb66602b7f95456c0257bcb3b3e1.jpg', '', 0, 'Standard:6:3', 0),
(26, 'Barca Jersey', '1600', '1800', 3, '70', '/tutorial/images/products/541f14c25b499a555857cbb7aa97f264.png', 'Nice Jersey', 0, 'M:2:1', 0),
(27, 'Basketball Jersey ', '1000', '1300', 3, '79', '/tutorial/images/products/71e6af62055b9efbd77a6f63ed72bc09.jpg', 'Just Out Now', 0, 'L:4:2,M:2:2,S:5:2', 0),
(28, 'Cricket Jersey', '1600', '1800', 4, '75', '/tutorial/images/products/d433b83490b9106d3994b2f7c23e185a.png', '', 0, 'M:7:1,L:5:2', 0),
(29, 'fifa-18', '1600', '1800', 7, '55', '/tutorial/images/products/607b8acd5f00f8bb4a99baf457c4e933.jpg', 'Now available ', 0, 's:5:2', 0),
(30, 'Baseball Bat', '2000', '2300', 5, '99', '', 'Nice one', 0, 'Small:4:2', 0),
(31, 'Football keeper gloves', '1500', '1800', 4, '63', '/tutorial/images/products/521a66cc510c16f8ed25d3fcb9985780.jpg', 'High grip football keeper gloves', 1, 'large:4:5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(175) NOT NULL,
  `state` varchar(175) NOT NULL,
  `city` varchar(175) NOT NULL,
  `street` varchar(175) NOT NULL,
  `phone` int(11) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `description` int(11) NOT NULL,
  `txn_type` varchar(255) NOT NULL,
  `txn_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `cart_id`, `full_name`, `email`, `state`, `city`, `street`, `phone`, `sub_total`, `tax`, `grand_total`, `description`, `txn_type`, `txn_date`) VALUES
(1, 53, 'sagar phuyal', 'sagar@gmail.com', '3', 'ktm', 'mulpani-6', 984444444, '1600.00', '0.00', '1600.00', 1, 'COD', '2018-01-14 15:23:22'),
(2, 54, 'sagar phuyal', 'sagar@gmail.com', '3', 'ktm', 'mulpani-6', 2147483647, '15706.00', '0.00', '15706.00', 6, 'COD', '2018-01-14 21:39:32'),
(3, 59, 'Om Sapkota', 'om@gmail.com', '2', 'ktm', 'mulpani-6', 124424521, '6120.00', '0.00', '6120.00', 3, 'COD', '2018-01-15 12:53:00'),
(4, 61, 'sagar phuyal', 'sagar@gmail.com', '2', 'ktm', 'mulpani-6', 2147483647, '5000.00', '0.00', '5000.00', 2, 'COD', '2018-01-15 17:05:08'),
(5, 0, '', '', '', '', '', 0, '0.00', '0.00', '0.00', 0, 'COD', '2018-01-15 17:35:52'),
(6, 62, 'Leo', 'Messi@gmail.com', '2', 'ktm', 'jorpati', 1111111111, '16666.00', '0.00', '16666.00', 4, 'COD', '2018-01-15 21:42:25'),
(7, 63, 'sagar phuyal', 'sagar@gmail.com', '3', 'ktm', 'mulpani-6', 2147483647, '41530.00', '0.00', '41530.00', 14, 'COD', '2018-01-16 10:04:34'),
(8, 64, 'sagar phuyal', 'ranjanm014369@nec.edu.np', '2', 'ktm', 'mulpani-6', 2147483647, '1600.00', '0.00', '1600.00', 1, 'COD', '2018-01-16 10:06:36'),
(9, 65, 'sagar phuyal', 'sagar@gmail.com', '2', 'ktm', 'mulpani-6', 2147483647, '8500.00', '0.00', '8500.00', 3, 'COD', '2018-01-16 10:36:45'),
(10, 66, 'Om Sapkota', 'om@gmail.com', '3', 'ktm', 'mulpani-6', 984444444, '16533.00', '0.00', '16533.00', 6, 'COD', '2018-01-20 09:34:02'),
(11, 68, 'loe messi', 'leo@gmail.com', 'no 3', 'ktm', 'jorpati', 1234567890, '1600.00', '0.00', '1600.00', 1, 'COD', '2018-06-15 07:45:46'),
(12, 69, 'A', 'leo@gmail.com', 'no 3', 'ktm', 'jorpati', 1234567890, '6133.00', '0.00', '6133.00', 3, 'COD', '2018-07-10 10:07:13'),
(13, 70, 'A', 'leo@gmail.com', 'no 3', 'ktm', 'jorpati', 1234567890, '1000.00', '0.00', '1000.00', 1, 'COD', '2018-07-10 10:10:51'),
(14, 73, 'sagar', 'sagarphuyal@yahoo.com', 'no 3', 'ktm', 'jorpati', 1234567890, '6833.00', '0.00', '6833.00', 2, 'COD', '2018-07-13 13:05:56'),
(15, 74, 'sagar', 'sagarphuyal@yahoo.com', 'no 3', 'ktm', 'jorpati', 1234567890, '5000.00', '0.00', '5000.00', 1, 'COD', '2018-07-13 13:20:52'),
(16, 76, 'loe messi', 'ranjanm014369@nec.edu.np', 'sdfdd', 'dddd', 'dd', 334, '5000.00', '0.00', '5000.00', 1, 'COD', '2018-08-09 09:38:08'),
(17, 77, 'hg', 'sagarphuyal44@gmail.com', '65', '545', '5445', 0, '4500.00', '0.00', '4500.00', 2, 'COD', '2018-08-09 11:31:34'),
(18, 77, 'hg', 'sagarphuyal44@gmail.com', '65', '545', '5445', 0, '4500.00', '0.00', '4500.00', 2, 'COD', '2018-08-09 11:32:02'),
(19, 78, 'sagar', 'leo@gmail.com', 'sdfdd', 'ktm', 'jorpati', 1234567890, '4500.00', '0.00', '4500.00', 1, 'COD', '2018-08-09 12:49:05'),
(20, 81, 'sdfghjn', 'admin@admin.com', 'asd', 'asd', 'asd', 0, '2500.00', '0.00', '2500.00', 1, 'COD', '2018-11-20 12:31:07'),
(21, 81, 'sdfghjn', 'admin@admin.com', 'asd', 'asd', 'asd', 0, '2500.00', '0.00', '2500.00', 1, 'COD', '2018-11-20 13:37:03'),
(22, 82, 'ranjan', 'rmanandhar87@gmail.com', '3', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 234, '6500.00', '0.00', '6500.00', 4, 'COD', '2018-11-22 15:11:42'),
(23, 82, 'ranjan', 'rmanandhar87@gmail.com', '3', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 234, '6500.00', '0.00', '6500.00', 4, 'COD', '2018-11-22 15:16:46'),
(24, 82, 'ranjan', 'rmanandhar87@gmail.com', '3', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 234, '6500.00', '0.00', '6500.00', 4, 'COD', '2018-11-22 15:18:11'),
(25, 82, 'ranjan', 'rmanandhar87@gmail.com', '3', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 234, '6500.00', '0.00', '6500.00', 4, 'COD', '2018-11-22 15:18:18'),
(26, 83, 'sdfg', 'rmanandhar87@gmail.com', 'Bhaktapur', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 34, '2000.00', '0.00', '2000.00', 2, 'COD', '2018-11-22 15:19:08'),
(27, 83, 'sdfg', 'rmanandhar87@gmail.com', 'Bhaktapur', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 34, '2000.00', '0.00', '2000.00', 2, 'COD', '2018-11-22 15:20:06'),
(28, 83, 'sdfg', 'rmanandhar87@gmail.com', 'Bhaktapur', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 34, '2000.00', '0.00', '2000.00', 2, 'COD', '2018-11-22 15:21:37'),
(29, 83, 'sdfg', 'rmanandhar87@gmail.com', 'Bhaktapur', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 34, '2000.00', '0.00', '2000.00', 2, 'COD', '2018-11-22 15:22:45'),
(30, 84, 'ghjk', 'admin@admin.com', 'Bhaktapur', 'Bhaktapur', 'jamune-7,Dhanyare,Tanahun, Duwakot,Bhaktapur', 56789, '2000.00', '0.00', '2000.00', 1, 'COD', '2018-11-22 15:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `join_date`, `last_login`, `permissions`) VALUES
(1, 'Ranjan Manandhar', 'ranjanm014369@nec.edu.np', '$2y$10$6kvVIqgFq3YzpffkKThu.ulBlX/oN1jgTSEF8pZLPoa6fCXqXwerW', '2017-10-17 18:35:03', '2018-11-22 10:25:35', 'admin,editor'),
(5, 'sagar phuyal', 'sagarphuyal44@gmail.com', '$2y$10$WC4c82r4hTxrCA0NAFY7MOHvoLL9GHPXSESZbhvqBI8hvWR7YKMPa', '2018-01-15 21:34:53', '2018-01-15 16:51:42', 'admin,editor'),
(6, 'om sapkota', 'om@gmail.com', '$2y$10$CDBNbVRJ5FO1LixEFQpjReA.oBd02Xw.0w1rfIiOTZxl/Tc0yLYCC', '2018-01-15 21:35:54', '2018-01-15 16:51:14', 'editor'),
(7, 'pk', 'pk@gmail.com', '$2y$10$hsO8xM1ugbjNkQyEoltz9OsuC.JW0685nZoe.UTDF0OFOMBJ2juKu', '2018-08-09 09:39:48', '2018-08-09 05:55:04', 'editor');

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE `users1` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(256) NOT NULL,
  `user_last` varchar(256) NOT NULL,
  `user_email` varchar(256) NOT NULL,
  `user_uid` varchar(256) NOT NULL,
  `user_pwd` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users1`
--
ALTER TABLE `users1`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users1`
--
ALTER TABLE `users1`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
