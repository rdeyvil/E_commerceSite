<?php
require_once 'C:/xampp/htdocs/tutorial/core/init.php';
$id = $_POST['id'];
$id = (int)$id;
$sql = "SELECT * FROM products WHERE id='$id'";
$result = $db->query($sql);
$product = mysqli_fetch_assoc($result);
$brand_id = $product['brand'];
$pcategory_id = $product['categories'];
$sql = "SELECT * FROM categories";
$result = $db->query($sql);
$category = mysqli_fetch_assoc($result);
$category_id = $category['id'];
$parent_id = $category['parent'];
$sql = "SELECT brand FROM  brand WHERE id ='$brand_id'";
$brand_query = $db->query($sql);
$brand = mysqli_fetch_assoc($brand_query);
$sizestring = $product['sizes'];
$sizestring = rtrim($sizestring,',');
$size_array = explode(',', $sizestring);
//recomendation part
$sql = "SELECT * FROM products WHERE categories = '$pcategory_id' ANd id != '$id' ORDER BY rand() LIMIT 4";
$featured = $db->query($sql);
?>
<?php ob_start(); ?>
<div class="modal  details-1" id="details-modal1" tabindex="-1" role="dialog" aria-labelledby="details-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="modal-header">
			<button class="close" type="button" onclick ="closemodalr()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center"><?= $product['title'];  ?></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<span id="modal_errors" class="bg-danger"></span>
						<div class="col-sm-6">
							<div class="center-block">
							<img src="<?= $product['image']; ?>" alt="<?= $product['title']; ?>" class="details img-responsive">
							</div>
						</div>
						<div class="col-sm-6">
							<h4>Details</h4>
							<p><?= nl2br($product['description']); ?></p>
							<hr>
							<p>Price Rs:<?= $product['price']; ?></p>
							<p>Brand::<?= $brand['brand']; ?></p>
							<button class="close1 btn btn-default" onclick="closemodalr()">Close</button>
							<button class="btn btn-warning" data-toggle="modal" onclick="detailsmodal(<?=$product['id']; ?>)" data-dismiss="modal"><span class="glyphicon glyphicon-shopping-cart"></span>Cart</button>
						</div>	
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<h2 class="text-center text-top" >Recommended for you</h2>
					<?php while($product= mysqli_fetch_assoc($featured)):?>
						<div class="col-md-3">
							<h4><?php echo $product['title']; ?></h4>
							<img src="<?php echo $product['image']; ?>" alt="<?php echo $product['title']; ?>" class="img-thumb"/><br>
							<!-- <button type="botton" class="btn btn-sm btn-success" data-toggle="modal" onclick="recommendation_modal(<?php echo $product['id']; ?>)" data-dismiss="modal">Details</button> -->
			<button class="btn btn-warning" style="position: relative;padding-left: 20px;" data-toggle="modal" onclick="detailsmodal(<?php echo $product['id']; ?>)" data-dismiss="modal"><span class="glyphicon glyphicon-shopping-cart"></span> Add To Cart</button>
						</div>
					<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>

<script>
	jQuery('#size').change(function(){
		var available = jQuery('#size option:selected').data("available");
		jQuery('#available').val(available);
	});
	function closemodalr() 
	{
		jQuery('#details-modal1').modal('hide');
		setTimeout(function(){
			jQuery('#details-modal1').remove();
			jQuery('.modal-backdrop').remove();
		},200);
	}
	function detailsmodalr(id){
		var data={"id" : id};
		jQuery.ajax({
		url: '/tutorial/includes/detailsmodal.php',
		method : "post",
		data : data,
		success : function(data){
				jQuery('body').append(data);
			jQuery('#details-modal1').modal('toggle');
		},
		error : function(){
			alert ("Something went wrong");
		}
	});
	}

	function recommendation_modalr(id){
		var data={"id" : id};
		jQuery.ajax({
		url: '/tutorial/includes/recommendation_modal.php',
		method : "post",
		data : data,
		success : function(data){
		jQuery('body').append(data);
		jQuery('#details-modal1').modal('toggle');

			
		
		},
		error : function(){
			alert ("Something went wrong");
		}
	});
	}

	function update_cart(mode,edit_id,edit_size){
 var data = {"mode" : mode, "edit_id" : edit_id, "edit_size" : edit_size};
 jQuery.ajax({
  url : '/tutorial/admin/parsers/update_cart.php',
  method : "post",
  data : data,
  success : function(){
  	location.reload();
  },
  error : function(){alert("Something went wrong.");},
  });
}

function add_to_cart(){
	jQuery('#modal_errors').html("");
	var size = jQuery('#size').val();
	var quantity = jQuery('#quantity').val();
	var available = jQuery('#available').val();
	var error = '';
	var data = jQuery('#add_product_form').serialize();
	if (size=='' || quantity == '' || quantity == 0) {
		error += '<p class="text-center text-danger">You must choose a size and quantity!!!</p>';
		jQuery('#modal_errors').html(error);
		return;
	}else if(quantity > available){
		error += '<p class="text-center text-danger">There are only '+available+' available!!!</p>';
		jQuery('#modal_errors').html(error);
		return;
	}else{
		jQuery.ajax({
			url : '/tutorial/admin/parsers/add_cart.php',
			method : 'post',
			data : data,
			success : function(){
				location.reload();
			},
			error : function(){alert("Something went wrong!!")}
		}); 
	}
}

</script>
<?php 
echo ob_get_clean(); ?>
