<?php
require_once 'C:/xampp/htdocs/tutorial/core/init.php';
$id = $_POST['id'];
$id = (int)$id;
$sql = "SELECT * FROM products WHERE id='$id'";
$result = $db->query($sql);
$product = mysqli_fetch_assoc($result);
$brand_id = $product['brand'];
$sql = "SELECT brand FROM  brand WHERE id ='$brand_id'";
$brand_query = $db->query($sql);
$brand = mysqli_fetch_assoc($brand_query);
$sizestring = $product['sizes'];
$sizestring = rtrim($sizestring,',');
$size_array = explode(',', $sizestring);
?>
<?php ob_start(); ?>
<div class="modal details-1" id="details-modal" tabindex="-1" role="dialog" aria-labelledby="details-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button class="close" type="button" onclick ="closemodal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center"><?= $product['title'];  ?></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<span id="modal_errors" class="bg-danger"></span>
						<div class="col-sm-6">
							<div class="center-block">
							<img src="<?= $product['image']; ?>" alt="<?= $product['title']; ?>" class="details img-responsive">
							</div>
						</div>
						<div class="col-sm-6">
							<h4>Details</h4>
							<p><?= nl2br($product['description']); ?></p>
							<hr>
							<p>Price Rs:<?= $product['price']; ?></p>
							<p>Brand::<?= $brand['brand']; ?></p>
							<form action="add_cart.php" method="post" id="add_product_form">
								<input type="hidden" name="product_id" value="<?=$id;?>">
								<input type="hidden" name="available" id="available" value="">
								<div class="form-group">
									<div class="col-xs-3">
										<label for="quantity">QUANTITY:</label>
										<input type="number" class="form-control" id="quantity" name="quantity" min="0">
									</div><div class="col-sx-9">&nbsp;</div>
								</div><br><br>
								<div class="form-group"> 
								<label for="size">Size</label>
								<select name="size" id="size" class="form-control">
									<option value=""></option>
								<?php foreach($size_array as $string){
									$string_array = explode(':',$string);
									$size = $string_array[0];
									$available = $string_array[1];
									if($available > 0){
									echo '<option value="'.$size.'" data-available="'.$available.'">'.$size.'('.$available.'Available)</option>';
									}} ?>
									
								</select>
								</div>
							</form>
						</div>	
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="close1 btn btn-default" onclick="closemodal()">Close</button>
				<button class="btn btn-warning" data-toggle="modal" onclick="add_to_cart();return false;"><span class="glyphicon glyphicon-shopping-cart"></span>Add to cart</button>

			</div>
		</div>
	</div>
</div>

<script>
	jQuery('#size').change(function(){
		var available = jQuery('#size option:selected').data("available");
		jQuery('#available').val(available);
	});
	
	function closemodal() 
	{
		jQuery('#details-modal').modal('hide');
		setTimeout(function(){
			jQuery('#details-modal').remove();
			jQuery('.modal-backdrop').remove();
		},200);
	}
	
function detailsmodal(id){
		var data={"id" : id};
		jQuery.ajax({
		url: '/tutorial/includes/detailsmodal.php',
		method : "post",
		data : data,
		success : function(data){
			jQuery('body').append(data);
			jQuery('#details-modal').modal('toggle');
		},
		error : function(){
			alert ("Something went wrong");
		}
	});
	}

	// function recommendation_modal(id){
	// 	var data={"id" : id};
	// 	jQuery.ajax({
	// 	url: '/tutorial/includes/recommendation_modal.php',
	// 	method : "post",
	// 	data : data,
	// 	success : function(data){
	// 		jQuery('body').append(data);
	// 		jQuery('#details-modal').modal('toggle');
	// 	},
	// 	error : function(){
	// 		alert ("Something went wrong");
	// 	}
	// });
	// }


function update_cart(mode,edit_id,edit_size){
 var data = {"mode" : mode, "edit_id" : edit_id, "edit_size" : edit_size};
 jQuery.ajax({
  url : '/tutorial/admin/parsers/update_cart.php',
  method : "post",
  data : data,
  success : function(){
  	location.reload();
  },
  error : function(){alert("Something went wrong.");},
  });
}

function add_to_cart(){
	jQuery('#modal_errors').html("");
	var size = jQuery('#size').val();
	var quantity = jQuery('#quantity').val();
	var available = jQuery('#available').val();
	var error = '';
	var data = jQuery('#add_product_form').serialize();
	if (size=='' || quantity == '' || quantity == 0) {
		error += '<p class="text-center text-danger">You must choose a size and quantity!!!</p>';
		jQuery('#modal_errors').html(error);
		return;
	}else if(quantity > available){
		error += '<p class="text-center text-danger">There are only '+available+' available!!!</p>';
		jQuery('#modal_errors').html(error);
		return;
	}else{
		jQuery.ajax({
			url : '/tutorial/admin/parsers/add_cart.php',
			method : 'post',
			data : data,
			success : function(){
				location.reload();
			},
			error : function(){alert("Something went wrong!!")}
		}); 
	}
}

</script>
<?php echo ob_get_clean(); ?>