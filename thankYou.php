<?php
require_once 'core/init.php';



//Get the contact informaion
$full_name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$state = sanitize($_POST['state']);
$city = sanitize($_POST['city']);
$street = sanitize($_POST['street']);
$phone = sanitize($_POST['phone']);
$tax = sanitize($_POST['tax']);
$sub_total = sanitize($_POST['sub_total']);
$grand_total = sanitize($_POST['grand_total']);
$cart_id = sanitize($_POST['cart_id']);
$description = sanitize($_POST['description']);
$metadata = array(
	"cart_id" =>$cart_id,
	"$tax" => $tax,
	"$sub_total" => $sub_total
);

//adjust inventory
$itemQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
$iresults = mysqli_fetch_assoc($itemQ);
$items = json_decode($iresults['items'],true);
foreach ($items as $item) {
	$newSizes = array();
	$item_id = $item['id'];
	$productQ = $db->query("SELECT sizes FROM products WHERE id = '{$item_id}'");
	$product = mysqli_fetch_assoc($productQ);
	$sizes = sizesToArray($product['sizes']);
	foreach ($sizes as $size) {
	 	if($size['size'] == $item['size']){
	 		$q = $size['quantity'] - $item['quantity'];
	 		$newSizes[] = array('size' => $size['size'],'quantity' => $q,'threshold' => $size['threshold']);
	 	}else{
	 		$newSizes[] = array('size' => $size['size'],'quantity' => $size['quantity']);
	 	}
	 } 
	 $sizeString = sizesToString($newSizes);
	 $db->query("UPDATE products SET sizes = '{$sizeString}' WHERE id = '{$item_id}'");
}

//update cart

$db->query(	"INSERT INTO transactions(cart_id,full_name,email,state,city,street,phone,sub_total,tax,grand_total,description,txn_type) VALUES('$cart_id','$full_name','$email','$state','$city','$street','$phone','$sub_total','$tax','$grand_total','$description','COD') ");
$domain = ($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false;
setcookie(CART_COOKIE,'',1,"/",$domain,false);
include 'includes/head.php';
include 'includes/navigation.php';
include 'includes/headerfull.php';
?>
<h1 class="text-center text-success">THANK YOU</h1>
<p>Your order has been successfully placed. Please check your email for conformation.</p>
<p>Your transaction number is<strong><?= $cart_id; ?></strong></p>
<p>Your order will be shipped to you in the next 2 working dayss to the folliwing address.</p>
<address>
	<strong>FULL NAME:</strong> <?=$full_name; ?><br>
	<strong>EMAIL:</strong>  <?=$email; ?><br>
	<strong>STATE: </strong> <?=$state; ?><br>
	<strong>CITY: </strong> <?=$city; ?><br>
	<strong>STREET:</strong>  <?=$street; ?><br>
	<strong>PHONE: </strong> <?=$phone; ?><br>
</address>

<?php
include 'includes/footer.php';
?>
