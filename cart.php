<?php
require_once 'core/init.php';
include 'includes/head.php'; 
include 'includes/navigation.php'; 
include 'includes/headerfull.php';


if($cart_id != ''){
    $cartQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
    $result = mysqli_fetch_assoc($cartQ);
    $items = json_decode($result['items'],true);
    $i = 1;
    $sub_total = 0;
    $item_count = 0;
}
?>
<div class="col-md-12">
    <div class="row">
        <h2 class="text-center">My Shopping Cart</h2><hr>
        <?php if ($cart_id == ''): ?>
            <div class="bg-danger">
                <p class="text-center text-danger">
                    Your Shopping Cart is Empty!!
                </p>
            </div>
        <?php else: ?>
            <table class="table table-bordered table-stripped table-condensed">
                <thead><th>#</th><th>Item</th><th>Price</th><th>Quantity</th><th>Size</th><th>Sub Total</th></thead>
                <tbody>
                    <?php
                        foreach ($items as $item) {
                            $product_id = $item['id'];  
                            $productQ = $db->query("SELECT * FROM products WHERE id = '{$product_id}'");
                            $product = mysqli_fetch_assoc($productQ);
                            $sArray = explode(',',$product['sizes']);
                            foreach ($sArray as $sizeString) {
                                $s = explode(':',$sizeString);
                                if ($s[0] == $item['size']) {
                                    $available = $s[1];
                                }
                            }

                    ?>

                    <tr>
                        <td><?= $i; ?></td>
                        <td><?= $product['title']; ?></td>
                        <td><?= money($product['price']); ?></td>
                        <td>
                         <button class="btn btn-default" onclick="update_cart('removeone','<?=$product['id']; ?>','<?=$item['size']; ?>');">-</button>
                             <?=$item['quantity']; ?>
                            <?php if($item['quantity'] < $available): ?> 
                            <button class="btn btn-default" onclick="update_cart('addone','<?=$product['id']; ?>','<?=$item['size']; ?>');">+</button>
                            <?php else: ?>
                            <span class="text-danger">Max</span>
                            <?php endif; ?>
                        </td>


                        <td><?= $item['size']; ?></td>
                        <td><?= money($item['quantity'] * $product['price']); ?></td>
                    </tr>

                    <?php   
                    $i++;
                    $item_count += $item['quantity'];
                    $sub_total += ($product['price'] * $item['quantity']);
                }   
                    $tax = TAXRATE * $sub_total;
                    $tax = number_format($tax,2);
                    $grand_total = $tax + $sub_total;

                ?>  
                </tbody>
            </table>
            <table class="table table-bordered table-condensed text-right">
                <legend>TOTALS</legend>
                <thead class="totals-table-header text-center"><th class="text-center">Total Items</th><th class="text-center">Sub Total</th><th class="text-center">Tax</th><th class="text-center">Grand Total</th></thead>
                <tbody>
                    <tr>
                        <td><?= $item_count;?></td>
                        <td><?= money($sub_total); ?></td>
                        <td><?= money($tax); ?></td>
                        <td class="bg-success"><?= money($grand_total); ?></td>
                    </tr>
                </tbody>
            </table>
<!-- CHECKOUT BUTTON -->
<button type="button" class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#checkoutModal">
  <span class="glyphicon glyphicon-shopping-cart"> CheckOut>></span>
</button>

<!-- Modal -->
<div class="modal fade" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="checkoutModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="checkoutModalLabel">Shipping Address</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <form method="post" id="payment-form">
            <span class="bg-danger" id="payment-errors"></span>
            <input type="hidden" name="tax" value="<?=$tax; ?>">
            <input type="hidden" name="sub_total" value="<?=$sub_total; ?>">
            <input type="hidden" name="grand_total" value="<?=$grand_total;?>">
            <input type="hidden" name="cart_id" value="<?=$cart_id; ?>">
            <input type="hidden" name="description" value="<?=$item_count.' item'; ?>">
            <div id="step1" style="display:block;">
                <div class="form-group col-md-6">
                    <label for="full_name">Full Name:</label>
                    <input  class="form-control" id="full_name" type="text" name="full_name">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email:</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="form-group col-md-6">
                    <label for="state">State:</label>
                    <input type="text" name="state" class="form-control" id="state">
                </div>
                <div class="form-group col-md-6">
                    <label for="city">City:</label>
                    <input type="text" name="city" class="form-control" id="city">
                </div>
                
                <div class="form-group col-md-6">
                    <label for="street">Street Address:</label>
                    <input type="text" name="street" class="form-control" id="street">
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone No:</label>
                    <input type="number_format" name="phone" class="form-control" id="phone">
                </div>

            </div>
            <div id="step2" style="display:none;">
                <fieldset>
                <legend>Please select one of the following</legend>
                <div class="form-group">
                <button type="radio" align="cod" name="action" onclick="document.getElementById('payment-form').action='thankYou.php';">Cash On Delivery</button> 
                </form>
                <form action="https://ir-user.esewa.com.np/epay/main" method="POST">
                        <input value="<?= $grand_total; ?>" name="tAmt" type="hidden">
                        <input value="<?= $grand_total; ?>" name="amt" type="hidden">
                        <input value="0" name="txAmt" type="hidden">
                        <input value="0" name="psc" type="hidden">
                        <input value="0" name="pdc" type="hidden">
                        <input value="test_merchant" name="scd" type="hidden">
                        <input value="ee2c3ca1-696b-4cc5-a6be-2c40d929d453" name="pid" type="hidden">
                        <input value="http://merchant.com.np/page/esewa_payment_success?q=su" type="hidden" name="su">
                        <input value="http://merchant.com.np/page/esewa_payment_failed?q=fu" type="hidden" name="fu">
                        <button  type="submit">Esewa</button>
                    </form>

                    <form action="http://cse.necedu.org/epay/index.php" method="POST">
                        <input value="<?= $grand_total; ?>" name="tAmt" type="hidden">
                        <input value="<?= $grand_total; ?>" name="amt" type="hidden">
                        <input value="0" name="txAmt" type="hidden">
                        <input value="0" name="psc" type="hidden">
                        <input value="0" name="pdc" type="hidden">
                        <input value="test_merchant" name="scd" type="hidden">
                        <input value="ee2c3ca1-696b-4cc5-a6be-2c40d929d453" name="pid" type="hidden">
                        <input value="http://localhost/tutorial/thankYou.php" type="hidden" name="su">
                        <input value="http://merchant.com.np/page/esewa_payment_failed?q=fu" type="hidden" name="fu">
                        <button  type="submit">Online payment</button>
                    </form>
                <form method="post" id="payment-form">
                </div>
                </fieldset>
                <!-- <div class="form-group col-md-3">
                    <label for="name">Name on Card</label>
                    <input type="text" id="name" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="number">Card NO</label>
                    <input type="text" id="number" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label for="cvc">CVC</label>
                    <input type="text" id="cvc" class="form-control">
                </div>
                <div class="form-group col-md-2">
                    <label for="exp-month">Expire Month</label>
                    <select id="exp-month" class="form-control">
                        <option value=""></option>
                        <?php for($i=1;$i<13;$i++): ?>
                        <option value="<?= $i; ?>"><?= $i ?></option>
                    <?php endfor; ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="exp-year">Expire Year</label>
                    <select id="exp-year" class="form-control">
                        <option value=""></option>
                        <?php $yr = date("Y"); ?>
                        <?php for($i=0;$i<11;$i++): ?>
                            <option value="<?=$yr+$i;?>"><?=$yr+$i;?></option>
                        <?php endfor; ?>
                    </select>
                </div> -->
            </div>
        </div>
      </div>
      <div class="modal-footer" >
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="back_address();" id="back_button" style="display: none;"><< Back </button>
        <button type="button" class="btn btn-primary" onclick="check_address();" id="next_button">Next >></button>
        <button type="submit" class="btn btn-primary" id="checkout_button" style="display: none;">Checkout>></button>
        </form>
      </div>
    </div>
  </div>
</div>
        <?php endif; ?>
    </div>
</div>

<script>
function back_address(){
    jQuery('#payment-errors').html("");
    jQuery('#step1').css("display","block");
    jQuery('#step2').css("display","none");
    jQuery('#next_button').css("display","inline-block");
    jQuery('#back_button').css("display","none");
    jQuery('#checkout_button').css("display","none");
    jQuery('#checkoutModalLabel').html("Shipping Address");

}

    function check_address(){
        var data = {
            'full_name' : jQuery('#full_name').val(),
            'email' : jQuery('#email').val(),
            'state' : jQuery('#state').val(),
            'city' : jQuery('#city').val(),
            'street' : jQuery('#street').val(),
            'phone' : jQuery('#phone').val(),
         };
         jQuery.ajax({
            url : '/tutorial/admin/parsers/check_address.php',
            method : 'POST',
            data : data,
            success : function(data){
                if(data != 'passed'){
                    jQuery('#payment-errors').html(data);


                }
                if(data == 'passed'){
                    jQuery('#payment-errors').html("");
                    jQuery('#step1').css("display","none");
                    jQuery('#step2').css("display","block");
                    jQuery('#next_button').css("display","none");
                    jQuery('#back_button').css("display","inline-block");
                    jQuery('#checkout_button').css("display","inline-block");
                    jQuery('#checkoutModalLabel').html("Enter your Payment Method.");
                }
            },
            error : function(){alert("something went wrong!!");},
            });
    }
</script>


<?php

include 'includes/footer.php';
?>